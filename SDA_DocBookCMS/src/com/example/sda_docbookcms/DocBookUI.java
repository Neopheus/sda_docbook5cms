package com.example.sda_docbookcms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.annotation.WebServlet;

import org.apache.commons.io.FilenameUtils;

import com.google.gwt.user.client.ui.Widget;
import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.FilesystemContainer;
import com.vaadin.data.util.TextFileProperty;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.Page.Styles;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.VerticalSplitPanel;

@SuppressWarnings({ "serial", "unused" })
@Theme("DocBookUI")
@Title("DocBook 5 Content Management System")
@PreserveOnRefresh
public class DocBookUI extends UI {
	
	public static String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
	public static String filePathRoot = basepath + "\\WEB-INF\\";
	public static String filePathStart = basepath + "\\WEB-INF\\start.html";
	public static String filePathMessages = basepath + "\\WEB-INF\\Messages\\";
	public static String filepathHTML = basepath + "\\WEB-INF\\DocBooksHTML\\";
	public static String filepathXML = basepath + "\\WEB-INF\\DocBooksXML\\";
	public static String filepathPDF = basepath + "\\WEB-INF\\DocBooksPDF\\";
	public static String filepathXSD = basepath + "\\WEB-INF\\XSDs\\";
	public static String filepathXSL = basepath + "\\WEB-INF\\XSLTs\\";
	public static String activeUser = null;
	public static String activeFile = null;
	public static Styles styles = null;
	public static Label fileView = new Label("", ContentMode.HTML);

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = DocBookUI.class)
	public static class Servlet extends VaadinServlet {
	}
	
	@Override
	protected void init(VaadinRequest request) {
		//only for TESTING
		//DbHandler.fillDB();
		
		//get the css of the page to modify it
		styles = Page.getCurrent().getStyles();
				
		//searchFilter for filtering the fileList
		SearchFilter searchFilter = new SearchFilter();
		//file container
		//list and container to display files from a given directory
		FilesystemContainer files = new FilesystemContainer(new File(filepathHTML), searchFilter, false);
		Table fileList = new Table("", files);
		fileList.setColumnCollapsingAllowed(true);
		fileList.setColumnCollapsible("Name", false);
		fileList.setColumnCollapsed("Icon", true);
		fileList.setColumnCollapsed("Size", true);
		fileList.setColumnCollapsed("Last Modified", true);
		styles.add(".v-table-column-selector { height: 0px; width: 0px;}");
		fileView.setPropertyDataSource(new TextFileProperty(new File(filePathStart)));

		
		//########################
		//##### outside part #####
		//########################
				
		//Login secition
		//contains all components for the login process
		FormLayout loginForm = new FormLayout();
		loginForm.setId("loginForm");
		loginForm.setMargin(true);
		styles.add("#loginForm {padding-left: 50px}");
		Label loginLabel = new Label("Login");
		loginForm.addComponent(loginLabel);
		TextField mail = new TextField("E-Mail:");
		loginForm.addComponent(mail);
		PasswordField password = new PasswordField("Password:");
		loginForm.addComponent(password);
		//only for testing
		//mail.setValue("mail@d1.de");
		//password.setValue("123");
		HorizontalLayout loginButtons = new HorizontalLayout();
		Button loginButton = new Button("Login");
		loginButtons.addComponent(loginButton);
		Button clearLoginButton = new Button("Clear");
		loginButtons.addComponent(clearLoginButton);
		loginForm.addComponent(loginButtons);
		Label loginWarning = new Label("Incorrect login data!");
		loginWarning.setId("loginWarning");
		styles.add("#loginWarning { color: red; }");
		loginForm.addComponent(loginWarning);
		loginWarning.setVisible(false);
				
		//registration section
		//contains all components for the registration process
		FormLayout regForm = new FormLayout();
		loginForm.setId("regForm");
		loginForm.setMargin(true);
		styles.add("#regForm {padding-right: 50px}");
		Label regLabel = new Label("Registration");
		regForm.addComponent(regLabel);
		TextField mailField = new TextField("New E-Mail:");
		regForm.addComponent(mailField);
		PasswordField pw1Field = new PasswordField("New Password:");
		pw1Field.setId("pw1");
		regForm.addComponent(pw1Field);
		PasswordField pw2Field = new PasswordField("Repeat Password:");
		pw2Field.setId("pw2");
		regForm.addComponent(pw2Field);
		HorizontalLayout regButtons = new HorizontalLayout();
		Button regButton = new Button("Register");
		regButtons.addComponent(regButton);
		Button clearRegButton = new Button("Clear");
		regButtons.addComponent(clearRegButton);
		regForm.addComponent(regButtons);
		Label regWarning = new Label("");
		regWarning.setId("regWarning");
		styles.add("#regWarning { color: red; }");
		regForm.addComponent(regWarning);
		regWarning.setVisible(false);
				
		//outside
		//contains login and registration
		//is only visible if not logged in
		HorizontalLayout outside = new HorizontalLayout();
		outside.addComponent(loginForm);
		outside.addComponent(regForm);
		outside.setSizeFull();
		outside.setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);
		outside.setComponentAlignment(regForm, Alignment.MIDDLE_CENTER);

		
		//#######################
		//##### inside part #####
		//#######################
				
		//sessioninfo section
		//contains username and logout button
		FormLayout sessionForm = new FormLayout();
		Label sessionLabel = new Label("Hello "+activeUser);
		sessionForm.addComponent(sessionLabel);
		Button logoutButton = new Button("Logout");
		sessionForm.addComponent(logoutButton);
				
		//separator left side
		//horizontal row to separate sessioninfo from filebrowsing
		Label separatorL = new Label("<hr />",ContentMode.HTML);
				
		//File Browsing
		//displays the filelist and the searchfield
		VerticalLayout fileBrowse = new VerticalLayout();
		FormLayout searchForm = new FormLayout();
		Label searchLabel = new Label("Instant Search:");
		searchForm.addComponent(searchLabel);
		TextField search = new TextField("");
		searchForm.addComponent(search);
		fileBrowse.addComponent(searchForm);
		fileBrowse.addComponent(fileList);
		fileList.setSizeFull();
						
		//tools panel
		//for upload, download and deleting files
		HorizontalLayout toolbar = new HorizontalLayout();
		XMLReceiver receiver = new XMLReceiver();
		receiver.fileList = fileList;
		Upload upload = new Upload(null, receiver);
		upload.addSucceededListener(receiver);
		upload.addFailedListener(receiver);
		toolbar.addComponent(upload);
		Button download = new Button("Download");
		toolbar.addComponent(download);
		HorizontalLayout downloadAs = new HorizontalLayout();
		//xml
		Button downloadXML = new Button("XML");
		FileDownloader fdXML = new FileDownloader(new FileResource(new File("")));
		fdXML.extend(downloadXML);
		downloadAs.addComponent(downloadXML);
		//html
		Button downloadHTML = new Button("HTML");
		FileDownloader fdHTML = new FileDownloader(new FileResource(new File("")));
		fdHTML.extend(downloadHTML);
		downloadAs.addComponent(downloadHTML);
		//pdf
		Button downloadPDF = new Button("PDF");
		FileDownloader fdPDF = new FileDownloader(new FileResource(new File("")));
		fdPDF.extend(downloadPDF);
		downloadAs.addComponent(downloadPDF);
		//cancle
		Button downloadCancle = new Button("Cancle");
		downloadAs.addComponent(downloadCancle);
		toolbar.addComponent(downloadAs);
		downloadAs.setVisible(false);
		Button delete = new Button("Delete");
		toolbar.addComponent(delete);
		HorizontalLayout acceptDelete = new HorizontalLayout();
		Button deleteYes = new Button("Yes");
		acceptDelete.addComponent(deleteYes);
		Button deleteNo = new Button("No");
		acceptDelete.addComponent(deleteNo);
		toolbar.addComponent(acceptDelete);
		acceptDelete.setVisible(false);
				
		//separator right side
		//separates tools from fielview
		Label separatorR = new Label("<hr />",ContentMode.HTML);
				
		//inside
		//contains all coomponents from the loginstate
		//only visible if logged in
		HorizontalSplitPanel inside = new HorizontalSplitPanel();
		inside.setSplitPosition(250, Sizeable.Unit.PIXELS);
		inside.setLocked(true);
		VerticalLayout navi = new VerticalLayout();
		navi.addComponent(sessionForm);
		navi.addComponent(separatorL);
		navi.addComponent(fileBrowse);
		inside.addComponent(navi);
		VerticalLayout main = new VerticalLayout();
		main.addComponent(toolbar);
		main.addComponent(separatorR);
		main.addComponent(fileView);
		inside.addComponent(main);
		inside.setSizeFull();		
				
		//set main panel
		setContent(outside);
		
		
		//#########################
		//##### eventhandling #####
		//#########################
				
		//file display
		fileList.addValueChangeListener(new ValueChangeListener(){
			@Override
			public void valueChange(ValueChangeEvent event){
				activeFile = event.getProperty().getValue().toString();
				activeFile = activeFile.replace(filepathHTML, "");
				activeFile = FilenameUtils.removeExtension(activeFile);
				fileView.setVisible(true);
				fileView.setPropertyDataSource(new TextFileProperty((File) event.getProperty().getValue()));
				downloadAs.setVisible(false);
				download.setVisible(true);
			}
		});
		fileList.setImmediate(true);
		
		//login button
		loginButton.addClickListener(new Button.ClickListener() {	
			@Override
			public void buttonClick(ClickEvent event) {
				loginWarning.setVisible(false);
				if(DbHandler.checkLogin(mail.getValue(), password.getValue())){
					activeUser = mail.getValue();
					sessionLabel.setValue("Hello "+activeUser);
					setContent(inside);
				} else {
					loginWarning.setVisible(true);
				}
				mail.clear();
				password.clear();
			}
		});
		
		//reset button login
		clearLoginButton.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				loginWarning.setVisible(false);
				mail.clear();
				password.clear();
			}
		});
		
		//logout button
		logoutButton.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				activeUser = null;
				setContent(outside);
			}
		});
		
		//register button
		regButton.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				regWarning.setVisible(false);
				String pw1 = pw1Field.getValue();
				String pw2 = pw2Field.getValue();
				boolean mailStatus = checkMail(mailField.getValue());
				boolean pwStatus = (pw1.equals(pw2) && pw1!="");
				if(mailStatus && pwStatus){
					DbHandler.insertUser(mailField.getValue(), pw1Field.getValue());
					activeUser = mailField.getValue();
					sessionLabel.setValue("Hello "+activeUser);
					setContent(inside);
				} else {
					if(!mailStatus && !pwStatus){
						regWarning.setValue("Everything was wrong!!!");
					} else if(!mailStatus){
						regWarning.setValue("No valid e-Mail or already used!");
					} else if(!pwStatus){
						regWarning.setValue("Not matching passwords or empty!");
					}
					regWarning.setVisible(true);					
				}
				mailField.clear();
				pw1Field.clear();
				pw2Field.clear();
				styles.add("#pw1,#pw2 { background: white; border-color: lightGrey}");
			}
		});
		
		//reset button registration
		clearRegButton.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				mailField.clear();
				pw1Field.clear();
				pw2Field.clear();
				styles.add("#pw1,#pw2 { background: white; border-color: lightGrey}");
				regWarning.setVisible(false);
			}
		});
		
		//pw1 checker
		pw1Field.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				checkPasswords(pw1Field, pw2Field);
			}
		});
		pw1Field.setImmediate(true);
		
		//pw2 checker
		pw2Field.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				checkPasswords(pw1Field, pw2Field);
			}
		});
		pw2Field.setImmediate(true);
		
		//download button
		//sets the active file as resource for the FileDownloader
		download.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				if(activeFile!=null){
					fdXML.setFileDownloadResource(new FileResource(new File(filepathXML+activeFile+".xml")));
					fdHTML.setFileDownloadResource(new FileResource(new File(filepathHTML+activeFile+".html")));
					fdPDF.setFileDownloadResource(new FileResource(new File(filepathPDF+activeFile+".pdf")));
					download.setVisible(false);
					downloadAs.setVisible(true);
				}
			}
		});
		
		//cancle download
		downloadCancle.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				downloadAs.setVisible(false);
				download.setVisible(true);
			}
		});
		
		//download xml
		downloadXML.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				
			}
		});
		
		//delete button
		delete.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				if(activeFile!=null){
					delete.setVisible(false);
					acceptDelete.setVisible(true);					
				}
			}
		});
		
		//deleteYes button
		deleteYes.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				if(DbHandler.deletetFile(activeFile, activeUser)>0){
					new File(DocBookUI.filepathXML + activeFile + ".xml").delete();
					new File(DocBookUI.filepathHTML + activeFile + ".html").delete();
					activeFile = null;
					fileList.refreshRowCache();
					acceptDelete.setVisible(false);
					delete.setVisible(true);
					showMessage(HtmlMessages.message_delete);
				} else {
					// show message "not owner of file" or something else
					showMessage(HtmlMessages.error_delete);
				}
			}
		});
		
		//deleteNo button
		deleteNo.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				acceptDelete.setVisible(false);
				delete.setVisible(true);
			}
		});
		
		//search field:
		//if the user enters something into the textfield
		//the value is set immediately into the searchTerm variable
		//and the fileList is also updated immediately
		search.addTextChangeListener(new FieldEvents.TextChangeListener() {
			@Override
			public void textChange(TextChangeEvent event) {
				if(event.getText()!=null){
					searchFilter.searchTerm = event.getText();
					searchFilter.filtering = true;
				} else {
					searchFilter.searchTerm = null;
					searchFilter.filtering = false;
				}
				fileList.refreshRowCache();
			}
		});
	}
	
	
	//############################
	//##### generell methods #####
	//############################
	
	private boolean checkMail(String mail){
		String regex = "^(.+)@(.+)$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(mail);
		boolean mailExisting = DbHandler.checkUser(mail);
		return (matcher.matches() && !mailExisting);
	}
	
	private void checkPasswords(PasswordField pw1, PasswordField pw2){
		if(pw1.getValue().equals(pw2.getValue())){
			styles.add("#pw1,#pw2 { background: lightGreen; border-color: green;}");
		} else {
			styles.add("#pw1,#pw2 { background: lightSalmon; border-color: red;}");
		}
	}
	
	public static void showMessage(HtmlMessages message){
		fileView.setPropertyDataSource(new TextFileProperty(new File(filePathMessages+message+".html")));
	}
}