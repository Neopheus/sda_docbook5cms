package com.example.sda_docbookcms;

import java.io.File;
import java.io.FilenameFilter;

public class SearchFilter implements FilenameFilter {

	//searchTerm and filtering are set from the DocBookUI
	//they are set while the user enters something into the search textfield
	public String searchTerm = null;
	public boolean filtering = false;
	
	//standard method of a FilenameFilter
	//returns true or false depending on if a term is or is not contained in the file name
	@Override
	public boolean accept(File directory, String fileName) {
		if(filtering){
			if(fileName.contains(searchTerm)){
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
}
