package com.example.sda_docbookcms;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.vaadin.server.VaadinService;

public class PropReader {
	private static InputStream inputStream;
	private static String propertiesFile = "config.properties";
		
	public PropReader(){
		
	}
	
	//extract a property from a properiesfile
	//this version is used for properties on the webserver
	public static String getPropertie(String key){
		String value = null;
		try {
			Properties prop = new Properties();
			String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
			String filepath = basepath + "/WEB-INF/configs/";
			inputStream = new FileInputStream(filepath+propertiesFile);
			if(inputStream != null){
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("file not found");
			}
			value = prop.getProperty(key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return value;
	}
	
	//alternative method with local properties just for testing
	public static String _getPropertie(String key){
		Map<String, String> props = new HashMap<String, String>();
		props.put("jdbc_driver", "com.mysql.jdbc.Driver");
		props.put("db_url", "jdbc:mysql://localhost");
		props.put("user", "admin");
		props.put("password", "admin");
		props.put("schema", "SDA_CMS");		
		props.put("ssl", "false");		
		return props.get(key);
	}
}
