package com.example.sda_docbookcms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.commons.io.FilenameUtils;

import com.vaadin.ui.Table;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;;

@SuppressWarnings("serial")
public class XMLReceiver implements Receiver, SucceededListener, FailedListener {
	
	public String fileName = null;
	public String tempFile = null;
	public Table fileList = null;
	public boolean isXML = false;
	public boolean isExisting = false;
	public boolean isValid = false;
		
	
	//method which is called after the file is uploaded successfully
	@Override
	public void uploadSucceeded(SucceededEvent event) {
		if(checkFile()){
			File from = new File(tempFile);
			File to = new File(DocBookUI.filepathXML+fileName+".xml"); //possible to add author name to the filename
			from.renameTo(to);
			XslTransformation.transformXMLtoHTML(fileName);
			DbHandler.insertFile(fileName, DocBookUI.activeUser);
			DocBookUI.showMessage(HtmlMessages.message_upload);
		} else {
			// TODO add html error page
			DocBookUI.showMessage(HtmlMessages.error_upload);
		}
		fileName = null;
		tempFile = null;
		isXML = false;
		isExisting = false;
		isValid = false;
		fileList.refreshRowCache();
	}
	
	//method which is called after the upload is not successfull
	@Override
	public void uploadFailed(FailedEvent event){
		fileName = null;
		tempFile = null;
		isXML = false;
		isExisting = false;
		isValid = false;
		DocBookUI.showMessage(HtmlMessages.error_upload);
	}
	
	//creates an outputstrem for the upload
	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		fileName = filename;
		tempFile = DocBookUI.filePathRoot+"temp\\"+fileName;
		FileOutputStream outStream = null;
		try {
			outStream = new FileOutputStream(tempFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return outStream;
	}
	
	//check if a file is valid for inserting into db
	//checks for: is file xml? is docbook valid? is file already on server?
	public boolean checkFile(){
		DocBookValidator validator = new DocBookValidator();
		isValid = validator.validate(tempFile);
		isXML = FilenameUtils.isExtension(fileName, "xml");
		fileName = FilenameUtils.removeExtension(fileName);
		isExisting = DbHandler.checkFileExistance(fileName);
		return isValid&&isXML&&!isExisting;
	}

}
