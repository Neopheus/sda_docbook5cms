package com.example.sda_docbookcms;

public enum HtmlMessages {
	error_general, error_delete, error_upload,
	message_delete, message_upload
}
