package com.example.sda_docbookcms;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;

public class DbHandler {
	private static String jdbc_driver = PropReader.getPropertie("jdbc_driver");
	private static String db_url = PropReader.getPropertie("db_url");
	private static String schema = PropReader.getPropertie("schema");
	private static String user = PropReader.getPropertie("user");
	private static String password = PropReader.getPropertie("password");
	private static String ssl = "?useSSL="+PropReader.getPropertie("ssl");

	//constructor
	public DbHandler(){
		//nothing todo
	}
	
	//initialize schema and tables
	public static void init(){
		File dirXML = new File(DocBookUI.filepathXML);
		File dirHTML = new File(DocBookUI.filepathHTML);
		for(File file: dirXML.listFiles()) file.delete();
		for(File file: dirHTML.listFiles()) file.delete();
		dropSchema();
		createSchema();
		createUserTable();
		createFileTable();
	}
	
	@SuppressWarnings("unused")
	private static void getAllProperties(){
		jdbc_driver = PropReader.getPropertie("jdbc_driver");
		db_url = PropReader.getPropertie("db_url");
		schema = PropReader.getPropertie("schema");
		user = PropReader.getPropertie("user");
		password = PropReader.getPropertie("password");
		ssl = "?useSSL="+PropReader.getPropertie("ssl");
	}
	
	//closes a db connection
	public static void closeConnection(Statement state, Connection conn){
		try {
			if(state != null){
				state.close();
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		try {
			if(conn != null){
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	//drop the "sda_cms" schema
	public static void dropSchema(){
		Connection connection = null;
		Statement statement = null;
		try{
			Class.forName(jdbc_driver);
			connection = DriverManager.getConnection(db_url+ssl, user, password);
			statement = connection.createStatement();
			String sql = "DROP DATABASE " + schema;
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(statement, connection);
		}
	}
	
	//creates the "sda_cms" schema
	public static void createSchema(){
		Connection connection = null;
		Statement statement = null;
		try{
			Class.forName(jdbc_driver);
			connection = DriverManager.getConnection(db_url+ssl, user, password);
			statement = connection.createStatement();
			String sql = "CREATE DATABASE " + schema;
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			if(e.getErrorCode()== 1007){
				System.out.println("Schema already exists, query rejected");
			} else {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(statement, connection);
		}
	}
	
	//creates the "user" table
	public static void createUserTable(){
		Connection connection = null;
		Statement statement = null;
		try{
			Class.forName(jdbc_driver);
			connection = DriverManager.getConnection(db_url+"/"+schema+ssl, user, password);
			statement = connection.createStatement();
			String sql = "CREATE TABLE USERDATA (" +
						 "id INTEGER AUTO_INCREMENT, " +
						 "username VARCHAR(100) UNIQUE, " +
						 "password VARCHAR(30), " +
						 "PRIMARY KEY(id) " +
						 ")";
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			if(e.getErrorCode() == 1050){
				System.out.println("Table already exists, query rejected");
			} else {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(statement, connection);
		}
	}
	
	//creates the "files" table
	public static void createFileTable(){
		Connection connection = null;
		Statement statement = null;
		try{
			Class.forName(jdbc_driver);
			connection = DriverManager.getConnection(db_url+"/"+schema+ssl, user, password);
			statement = connection.createStatement();
			String sql = "CREATE TABLE FILES (" +
						 "id INTEGER AUTO_INCREMENT, " +
						 "filename VARCHAR(255) UNIQUE, " +
						 "author INTEGER, " +
						 "PRIMARY KEY(id), " +
						 "FOREIGN KEY(author) REFERENCES USERDATA(id) " +
						 ")";
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			if(e.getErrorCode() == 1050){
				System.out.println("Table already exists, query rejected");
			} else {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(statement, connection);
		}
	}
	
	//insert user into table
	public static void insertUser(String userName, String userPassword){
		Connection connection = null;
		Statement statement = null;
		try{
			Class.forName(jdbc_driver);
			connection = DriverManager.getConnection(db_url+"/"+schema+ssl, user, password);
			statement = connection.createStatement();
			String sql = "INSERT INTO USERDATA VALUES(0, '"+userName+"', '"+userPassword+"')";
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(statement, connection);
		}
	}
	
	//insert file into table
	public static void insertFile(String fileName, String fileAuthor){
		Connection connection = null;
		Statement statement = null;
		try{
			Class.forName(jdbc_driver);
			connection = DriverManager.getConnection(db_url+"/"+schema+ssl, user, password);
			statement = connection.createStatement();
			String sql = "INSERT INTO FILES VALUES(0, '"+fileName+"', '"+getUserID(fileAuthor)+"')";
			statement.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(statement, connection);
		}
	}
	
	//delete file from table
	public static int deletetFile(String fileName, String fileAuthor){
		Connection connection = null;
		Statement statement = null;
		try{
			Class.forName(jdbc_driver);
			connection = DriverManager.getConnection(db_url+"/"+schema+ssl, user, password);
			statement = connection.createStatement();
			String sql = "DELETE FROM FILES WHERE filename='"+fileName+"' AND author='"+getUserID(fileAuthor)+"'";
			return statement.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(statement, connection);
		}
		return 0;
	}
	
	//delete file from table
	public static boolean checkFileOwnership(String fileName, String fileAuthor){
		Connection connection = null;
		Statement statement = null;
		try{
			Class.forName(jdbc_driver);
			connection = DriverManager.getConnection(db_url+"/"+schema+ssl, user, password);
			statement = connection.createStatement();
			String sql = "SELECT COUNT(*) FROM FILES WHERE filename='"+fileName+"' AND author='"+getUserID(fileAuthor)+"'";
			ResultSet result = statement.executeQuery(sql);
			result.next();
			if(result.getInt(1)==1){
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(statement, connection);
		}
		return false;
	}

	public static boolean checkFileExistance(String fileName) {
		Connection connection = null;
		Statement statement = null;
		try{
			Class.forName(jdbc_driver);
			connection = DriverManager.getConnection(db_url+"/"+schema+ssl, user, password);
			statement = connection.createStatement();
			String sql = "SELECT COUNT(*) FROM FILES WHERE filename='"+fileName+"'";
			ResultSet result = statement.executeQuery(sql);
			result.next();
			if(result.getInt(1)==1){
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(statement, connection);
		}
		return false;
	}
	
	//check login
	public static boolean checkLogin(String userName, String userPassword){
		Connection connection = null;
		Statement statement = null;
		try{
			Class.forName(jdbc_driver);
			connection = DriverManager.getConnection(db_url+"/"+schema+ssl, user, password);
			statement = connection.createStatement();
			String sql = "SELECT COUNT(*) FROM USERDATA WHERE username='"+userName+"' AND password='"+userPassword+"'";
			ResultSet result = statement.executeQuery(sql);
			result.next();
			if(result.getInt(1)==1){
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(statement, connection);
		}
		return false;
	}
	
	//check user existence
	public static boolean checkUser(String userName){
		Connection connection = null;
		Statement statement = null;
		try{
			Class.forName(jdbc_driver);
			connection = DriverManager.getConnection(db_url+"/"+schema+ssl, user, password);
			statement = connection.createStatement();
			String sql = "SELECT COUNT(*) FROM USERDATA WHERE username='"+userName+"'";
			ResultSet result = statement.executeQuery(sql);
			result.next();
			if(result.getInt(1)==1){
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(statement, connection);
		}
		return false;
	}
	
	//get id from user
	public static String getUserID(String userName){
		Connection connection = null;
		Statement statement = null;
		try{
			Class.forName(jdbc_driver);
			connection = DriverManager.getConnection(db_url+"/"+schema+ssl, user, password);
			statement = connection.createStatement();
			String sql = "SELECT id FROM USERDATA WHERE username='"+userName+"'";
			ResultSet result = statement.executeQuery(sql);
			result.next();
			return result.getString("id");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection(statement, connection);
		}
		return null;
	}
	
	public static void fillDB(){
		DbHandler.init();
		for(int i = 0; i<20; i++){
			String user = "mail@d"+i+".de";
			DbHandler.insertUser(user, "123");
			//DbHandler.insertFile("file_"+i, user);
		}
	}
	
	//main method only for testing
	public static void main(String[] args){
		boolean fillDb = false;
		boolean dbTest = false;
		boolean propTest = false;
		
		
		if(fillDb){
			DbHandler.fillDB();
		}
		
		if(dbTest){
			String activeUser = null;
			DbHandler.init();
			
			DbHandler.insertUser("mail@neopheus.de", "xyz123");
			DbHandler.insertUser("neopheus1@gmx.de", "xyz123");
			DbHandler.insertUser("neopheus@gmx.net", "xyz123");
			DbHandler.insertUser("bm055@hdm-stuttgart.de", "xyz123");
			
			//check user
			System.out.println("user existing: " + checkUser("mail@neopheus.de"));
			
			//session1
			//login
			if(DbHandler.checkLogin("mail@neopheus.de", "xyz123")){
				activeUser = "mail@neopheus.de";
			} else {
				System.out.println("login failed");
			}
			//working
			if(activeUser!=null){
				DbHandler.insertFile("java for dummys", activeUser);
				DbHandler.insertFile("Just4Fun", activeUser);
				DbHandler.deletetFile("java for dummys", activeUser);
				activeUser = null;
			}
			
			//session2
			//login
			if(DbHandler.checkLogin("bm055@hdm-stuttgart.de", "xyz123")){
				activeUser = "bm055@hdm-stuttgart.de";
			} else {
				System.out.println("login failed");
			}
			//working
			if(activeUser!=null){
				DbHandler.deletetFile("Just4Fun", activeUser);
				activeUser = null;
			}
		}
		
		if(propTest){
			System.out.println(PropReader.getPropertie("user"));
			System.out.println(PropReader.getPropertie("password"));
			System.out.println(PropReader.getPropertie("schema"));
			System.out.println(PropReader.getPropertie("ssl"));
		}
	}
}
