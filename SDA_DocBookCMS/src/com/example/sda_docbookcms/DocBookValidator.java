package com.example.sda_docbookcms;

import java.io.File;
import java.io.IOException;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

public class DocBookValidator {
	
	public SchemaFactory schemaFactory = null;
	public Schema schema =  null;
	public Validator validator = null;
	
	//Constructor
	public DocBookValidator() {
		schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		try {
			schema = schemaFactory.newSchema(new File(DocBookUI.filepathXSD+PropReader.getPropertie("xsd")));
		} catch (SAXException e) {
			e.printStackTrace();
		}
		validator = schema.newValidator();
	}
	
	//Validator
	//validates a docbook
	public boolean validate(String path){
		StreamSource file = new StreamSource(new File(path));
		try {
			validator.validate(file);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}
