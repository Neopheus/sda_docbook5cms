package com.example.sda_docbookcms;

import javax.xml.transform.*;
import javax.xml.transform.stream.*;

public class XslTransformation {
	
	public static void transform( String inputXSL, String inputXML, String outfile ) throws Exception {
		Source xsl = new StreamSource( inputXSL );
		Source xml = new StreamSource( inputXML );
		Result out = new StreamResult( outfile  );
		Transformer transformer = TransformerFactory.newInstance().newTransformer( xsl );
		transformer.transform( xml, out );
	}
	
	public static void transformXMLtoHTML(String filename){
		String xslFilename = DocBookUI.filepathXSL+PropReader.getPropertie("xsl");
		String inFilename = DocBookUI.filepathXML+filename+".xml";
		String outFilename = DocBookUI.filepathHTML+filename+".html";
		try {
			transform(xslFilename, inFilename, outFilename);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
}