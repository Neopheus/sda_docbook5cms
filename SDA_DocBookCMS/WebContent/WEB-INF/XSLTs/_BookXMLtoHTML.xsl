<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="html"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" 
        doctype-public="-//W3C//DTD XHTML 1.0 strict//EN"/>
    
    <xsl:template match="/book">
        <html>
            <head>
                <title><xsl:value-of select="title"/></title>
            </head>
            <body>
                <h1><xsl:value-of select="title"/></h1>
                <ul>
                    <xsl:for-each select="chapter/title">
                        <li><a><xsl:attribute name="href">#<xsl:value-of select="../@id"/></xsl:attribute><xsl:value-of select="."/></a></li>
                    </xsl:for-each>
                </ul>
                <xsl:apply-templates select="chapter"/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="chapter">
        <div>
            <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
            <hr/>
            <h2><xsl:value-of select="title"/></h2>
            <xsl:apply-templates select="para|itemizedlist|table"/>
            <hr/>
        </div>
    </xsl:template>
    
    <xsl:template match="para">
        <p><xsl:value-of select="."/></p>
    </xsl:template>
    
    <xsl:template match="itemizedlist">
        <ul>
            <xsl:apply-templates select="listitem"/>
        </ul>
    </xsl:template>
    
    <xsl:template match="listitem">
        <li>
            <xsl:apply-templates select="para|itemizedlist"/>
        </li>
    </xsl:template>
    
    <xsl:template match="table">
        <table>
            <xsl:attribute name="border"><xsl:value-of select='@border'/></xsl:attribute>
            <caption><xsl:value-of select="caption"/></caption>
            <xsl:apply-templates select="tr"/>
        </table>
    </xsl:template>
    
    <xsl:template match="tr">
        <tr>
            <xsl:apply-templates select="td"/>
        </tr>
    </xsl:template>
    
    <xsl:template match="td">
        <td><xsl:attribute name="colspan"><xsl:value-of select='@colspan'/></xsl:attribute>
            <xsl:attribute name="rowspan"><xsl:value-of select='@rowspan'/></xsl:attribute>
            <xsl:value-of select="."/></td>
    </xsl:template>
    
</xsl:stylesheet>